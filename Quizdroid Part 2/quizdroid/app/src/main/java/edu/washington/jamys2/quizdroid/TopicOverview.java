package edu.washington.jamys2.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;


public class TopicOverview extends ActionBarActivity {

    public static final int QUESTION_ONE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_overview);

        Intent launchedMe = getIntent();
        String itemValue = launchedMe.getStringExtra("topics");
        TextView topic_name = (TextView)findViewById(R.id.topic_id);
        topic_name.setText(itemValue);
        setInformation(itemValue);
    }

    private void setInformation(final String itemValue) {
        TextView description = (TextView)findViewById(R.id.description);
        TextView question_num = (TextView)findViewById(R.id.question_num);
        question_num.setText("2");

        if (itemValue.equals("Math")) {
            description.setText("Test your Mathematical abilities through a series of questions!" +
                    " At the end of the quiz, you will receive your results. Begin the quiz now!");
        } else if (itemValue.equals("Physics")) {
            description.setText("Are you confident in your physics knowledge? Well, what are you" +
                    " waiting for, start your test now!");
        } else if (itemValue.equals("Marvel Super Heroes")) {
            description.setText("Love Marvel? Enjoy the comics? The movies? Will you be willing" +
                    " to test your knowledge and confidence today? Begin now!!");
        } else {
            description.setText(itemValue);
        }

        Button b = (Button)findViewById(R.id.start);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextActivity = new Intent(TopicOverview.this, Questions.class);
                nextActivity.putExtra("topics", itemValue);
                nextActivity.putExtra("startQuestion", QUESTION_ONE);
                startActivity(nextActivity);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_topic_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
