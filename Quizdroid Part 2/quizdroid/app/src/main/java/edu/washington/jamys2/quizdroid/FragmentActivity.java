package edu.washington.jamys2.quizdroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class FragmentActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        Intent launchedMe = getIntent();
        String itemValue = launchedMe.getStringExtra("topics");
        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            TopicOverviewFragment overview = new TopicOverviewFragment();
            Bundle args = new Bundle();
            args.putString("topics", itemValue);
            overview.setArguments(args);
            fragmentTransaction.add(R.id.container, overview);
            fragmentTransaction.commit();
        }
    }

    // Topic Overview
    public class TopicOverviewFragment extends Fragment {
        public static final int QUESTION_ONE = 1;

        public TopicOverviewFragment() {
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_topic_overview, container, false);
            TextView topic_name = (TextView) rootView.findViewById(R.id.topic_id);
            TextView description = (TextView) rootView.findViewById(R.id.description);
            TextView question_num = (TextView) rootView.findViewById(R.id.question_num);
            Bundle args = getArguments();
            final String itemValue = args.getString("topics");
            topic_name.setText(itemValue);
            question_num.setText("2");

            switch (itemValue) {
                case "Math":
                    description.setText("Test your Mathematical abilities through a series of questions!" +
                            " At the end of the quiz, you will receive your results. Begin the quiz now!");
                    break;
                case "Physics":
                    description.setText("Are you confident in your physics knowledge? Well, what are you" +
                            " waiting for, start your test now!");
                    break;
                case "Marvel Super Heroes":
                    description.setText("Love Marvel? Enjoy the comics? The movies? Will you be willing" +
                            " to test your knowledge and confidence today? Begin now!!");
                    break;
                default:
                    description.setText(itemValue);
                    break;
            }

            Button b = (Button) rootView.findViewById(R.id.start);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getActivity().getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    QuestionsFragment question = new QuestionsFragment();
                    Bundle args = new Bundle();
                    args.putString("topics", itemValue);
                    args.putInt("startQuestion", QUESTION_ONE);
                    question.setArguments(args);
                    fragmentTransaction.replace(R.id.container, question);
                    fragmentTransaction.commit();
                }
            });
            return rootView;
        }
    }

    // Questions
    public class QuestionsFragment extends Fragment {
        int correct;
        int total;
        String[] math_questions;
        String[] physics_questions;
        String[] marvel_questions;
        String[] math_answers;
        String[] physics_answers;
        String[] marvel_answers;
        String[] math_correct;
        String[] physics_correct;
        String[] marvel_correct;
        TextView questionView;
        TextView answer1;
        TextView answer2;
        TextView answer3;
        TextView answer4;
        Button send;
        RadioGroup radioChoice;
        RadioButton radioChoiceButton;
        String itemValue;

        public QuestionsFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.activity_questions, container, false);
            Bundle args = getArguments();
            itemValue = args.getString("topics");
            int startQuestion = args.getInt("startQuestion", 1);

            math_questions = new String[]{"1 + 1 = ....", "10 - 1 = ...."};
            physics_questions = new String[]{"What scientist is well known for his theory of relativity?", "Earth is located in which galaxy?"};
            marvel_questions = new String[]{"Peter Parker works as a photographer for?", "Captain America was frozen in which war?"};

            math_answers = new String[]{"1", "2", "3", "4", "8", "9", "10", "11"};
            physics_answers = new String[]{"Albert Einstein", "Professor Xavier", "Charles Darwin", "None of the above", "Milky Way", "Andromeda", "Pinwheel", "Solar"};
            marvel_answers = new String[]{"The Daily Planet", "The Daily Bugle", "The New York Times", "The Rolling Stone", "World War I", "World War II", "Cold War", "American Civil War"};

            math_correct = new String[]{"2", "9"};
            physics_correct = new String[]{"Albert Einstein", "Milky Way"};
            marvel_correct = new String[]{"The Daily Bugle", "World War II"};

            send = (Button) rootView.findViewById(R.id.submit);
            questionView = (TextView) rootView.findViewById(R.id.question_title);
            answer1 = (TextView) rootView.findViewById(R.id.radio1);
            answer2 = (TextView) rootView.findViewById(R.id.radio2);
            answer3 = (TextView) rootView.findViewById(R.id.radio3);
            answer4 = (TextView) rootView.findViewById(R.id.radio4);

            send.setEnabled(false);
            radioChoice = (RadioGroup) rootView.findViewById(R.id.radioGroup);
            radioChoice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId > 0) {
                        send.setEnabled(true);
                    }
                }
            });

            if (startQuestion == 1) {
                startQuestion++;
                total = 0;
                correct = 0;
                switch (itemValue) {
                    case "Math":
                        questionView.setText(math_questions[0]);
                        answer1.setText(math_answers[0]);
                        answer2.setText(math_answers[1]);
                        answer3.setText(math_answers[2]);
                        answer4.setText(math_answers[3]);
                        break;
                    case "Physics":
                        questionView.setText(physics_questions[0]);
                        answer1.setText(physics_answers[0]);
                        answer2.setText(physics_answers[1]);
                        answer3.setText(physics_answers[2]);
                        answer4.setText(physics_answers[3]);
                        break;
                    case "Marvel Super Heroes":
                        questionView.setText(marvel_questions[0]);
                        answer1.setText(marvel_answers[0]);
                        answer2.setText(marvel_answers[1]);
                        answer3.setText(marvel_answers[2]);
                        answer4.setText(marvel_answers[3]);
                        break;
                }
            } else {
                Bundle change = getArguments();
                total = change.getInt("currentQuestion", 1);
                correct = change.getInt("totalCorrect", 1);
                switch (itemValue) {
                    case "Math":
                        checkMath();
                        break;
                    case "Physics":
                        checkPhysics();
                        break;
                    case "Marvel Super Heroes":
                        checkMarvel();
                        break;
                }
            }

            send.setOnClickListener(new View.OnClickListener() {
                String correctAnswer = "";

                @Override
                public void onClick(View v) {
                    total++;
                    int selectedId = radioChoice.getCheckedRadioButtonId();
                    radioChoiceButton = (RadioButton) rootView.findViewById(selectedId);
                    if (selectedId > 0) {
                        Toast.makeText(getApplicationContext(), "You have selected " + radioChoiceButton.getText(), Toast.LENGTH_SHORT).show();
                        switch (itemValue) {
                            case "Math":
                                correctAnswer = correct();
                                break;
                            case "Physics":
                                correctAnswer = correct();
                                break;
                            case "Marvel Super Heroes":
                                correctAnswer = correct();
                                break;
                        }
                        radioChoice.clearCheck();
                    }
                    FragmentManager fragmentManager = getActivity().getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    AnswerSummaryFragment summary = new AnswerSummaryFragment();
                    Bundle args = new Bundle();
                    args.putString("topics", itemValue);
                    args.putString("userAnswer", (String)radioChoiceButton.getText());
                    args.putString("correctAnswer", correctAnswer);
                    args.putInt("currentQuestion", total);
                    args.putInt("totalCorrect", correct);
                    args.putInt("startQuestion", 1);
                    summary.setArguments(args);
                    fragmentTransaction.replace(R.id.container, summary);
                    fragmentTransaction.commit();
                }
            });
            return rootView;
        }

        private void checkMath() {
            if ((total + 1) == math_questions.length) {
                questionView.setText(math_questions[total]);
                answer1.setText(math_answers[4]);
                answer2.setText(math_answers[5]);
                answer3.setText(math_answers[6]);
                answer4.setText(math_answers[7]);
            }
        }

        private void checkPhysics() {
            if ((total + 1) == physics_questions.length) {
                questionView.setText(physics_questions[total]);
                answer1.setText(physics_answers[4]);
                answer2.setText(physics_answers[5]);
                answer3.setText(physics_answers[6]);
                answer4.setText(physics_answers[7]);
            }
        }

        private void checkMarvel() {
            if ((total + 1) == marvel_questions.length) {
                questionView.setText(marvel_questions[total]);
                answer1.setText(marvel_answers[4]);
                answer2.setText(marvel_answers[5]);
                answer3.setText(marvel_answers[6]);
                answer4.setText(marvel_answers[7]);
            }
        }

        private String correct() {
            String[] answerCheck;
            switch (itemValue) {
                case "Math":
                    answerCheck = math_correct;
                    break;
                case "Physics":
                    answerCheck = physics_correct;
                    break;
                default:
                    answerCheck = marvel_correct;
                    break;
            }

            for (String anAnswerCheck : answerCheck) {
                if (anAnswerCheck.equals(radioChoiceButton.getText())) {
                    correct++;
                }
            }
            return answerCheck[total - 1];
        }
    }

    // Answer Summary
    public class AnswerSummaryFragment extends Fragment {
        public AnswerSummaryFragment(){
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_answer_summary, container, false);
            Bundle args = getArguments();
            final String itemValue = args.getString("topics");
            final String userAnswer = args.getString("userAnswer");
            final String correctAnswer = args.getString("correctAnswer");
            final int currentQuestion = args.getInt("currentQuestion", 1);
            final int totalCorrect = args.getInt("totalCorrect", 1);

            TextView userAnswerPlace = (TextView) rootView.findViewById(R.id.userAnswer);
            TextView correctAnswerPlace = (TextView) rootView.findViewById(R.id.correctAnswer);
            TextView currentQuestionPlace = (TextView) rootView.findViewById(R.id.questionNum);
            TextView totalCorrectPlace = (TextView) rootView.findViewById(R.id.numCorrect);
            userAnswerPlace.setText(userAnswer);
            correctAnswerPlace.setText(correctAnswer);
            currentQuestionPlace.setText("Question Number " + currentQuestion + " out of 2");
            totalCorrectPlace.setText("You have " + totalCorrect + " out of " + currentQuestion + " correct answers!");

            Button next = (Button) rootView.findViewById(R.id.next);
            if (currentQuestion == 2) {
                next.setText("Finish");
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent nextActivity = new Intent(getActivity(), MainActivity.class);
                        startActivity(nextActivity);
                    }
                });
            } else {
                next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                        QuestionsFragment question = new QuestionsFragment();
                        Bundle args = new Bundle();
                        args.putString("topics", itemValue);
                        args.putString("userAnswer", userAnswer);
                        args.putString("correctAnswer", correctAnswer);
                        args.putInt("currentQuestion", currentQuestion);
                        args.putInt("totalCorrect", totalCorrect);
                        args.putInt("startQuestion", 10);
                        question.setArguments(args);
                        fragmentTransaction.replace(R.id.container, question);
                        fragmentTransaction.commit();
                    }
                });
            }

            return rootView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
