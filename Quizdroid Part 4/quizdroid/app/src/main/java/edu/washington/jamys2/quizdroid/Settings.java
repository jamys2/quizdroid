package edu.washington.jamys2.quizdroid;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Anyone on 3/16/15.
 */
public class Settings extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
