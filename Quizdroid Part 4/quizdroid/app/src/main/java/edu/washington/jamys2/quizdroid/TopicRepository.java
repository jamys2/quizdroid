package edu.washington.jamys2.quizdroid;

import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/2015.
 */

public interface TopicRepository {

    public ArrayList<Quiz> getQuestions();

    public void setQuestions(ArrayList<Quiz> questions);

    public String getTitle();

    public void setTitle(String title);

    public String getShort();

    public void setShort(String shortDescription);

    public String getLong();

    public void setLong(String longDescription);

    public Quiz getCurrentQuestion();

    public int getQuestionNumber();

    public void incrementCurrentQuestion();

    public int getTotalCorrect();

    public void incrementTotalCorrect();
}
