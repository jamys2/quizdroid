package edu.washington.jamys2.quizdroid;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/2015.
 */
public class Topic implements Serializable {
    private ArrayList<Quiz> questions;
    private String title;
    private String shortDescription;
    private String longDescription;
    private int current;
    private int correct;

    public Topic() {
        questions = new ArrayList<Quiz>();
    }

    public ArrayList<Quiz> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Quiz> questions) {
        this.questions = questions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort() {
        return shortDescription;
    }

    public void setShort(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLong() {
        return longDescription;
    }

    public void setLong(String longDescription) {
        this.longDescription = longDescription;
    }

    public Quiz getCurrentQuestion() {
        return questions.get(current);
    }

    public int getQuestionNumber() {
        return current;
    }

    public void incrementQuestion() {
        this.current++;
    }

    public int getCorrect() {
        return correct;
    }

    public void incrementCorrect() {
        this.correct++;
    }
}
