package edu.washington.jamys2.quizdroid;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/2015.
 */
public class QuizApp extends Application {

    private static QuizApp instance;
    private PendingIntent pendingIntent;
    private TopicRepos repository;
    private ArrayList<String> preferences = new ArrayList<>();
    private boolean changed;
    private boolean on;

    @Override
    public void onCreate() {
        super.onCreate();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        preferences.add(sharedPref.getString("frequency", "5"));
        preferences.add(sharedPref.getString("downloadURL", ""));

        Intent alarmIntent = new Intent(QuizApp.this, AlarmReceiver.class);

        pendingIntent = PendingIntent.getBroadcast(QuizApp.this, 1,
                alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        start();

        this.repository = new TopicRepos();
        Log.d("QuizApp", "Application is running");
    }

    public QuizApp() {
        if (instance == null) {
            instance = this;
        } else {
            throw new RuntimeException("Multiple instances");
        }
    }

    public static QuizApp getInstance() {
        return instance;
    }

    public TopicRepos getRepository() {
        return repository;
    }

    public ArrayList<String> getPreferences() {
        return preferences;
    }

    public boolean getChanged() {
        return changed;
    }

    public void setPreferences(int i, String frequency) {
        preferences.set(i, frequency);
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public void setDownloading(boolean setting) {
        on = setting;
    }

    public boolean getDownloading() {
        return on;
    }

    public void start() {
        setDownloading(true);
        int interval = Integer.parseInt(preferences.get(0)) * 1000;// * 60; //Converts min to milli;

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
    }

    public void cancel() {
        setDownloading(false);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        pendingIntent.cancel();
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }
}
