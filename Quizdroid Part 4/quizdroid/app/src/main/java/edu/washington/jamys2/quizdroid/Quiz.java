package edu.washington.jamys2.quizdroid;

import java.io.Serializable;

/**
 * Created by Anyone on 3/16/2015.
 */
public class Quiz implements Serializable {

    private String[] answers;
    private String question;
    private int correct;

    public Quiz() {
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

}
