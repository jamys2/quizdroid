package edu.washington.jamys2.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class AnswerSummary extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_summary);
        Intent launchedMe = getIntent();
        String itemValue = launchedMe.getStringExtra("topics");
        String userAnswer = launchedMe.getStringExtra("userAnswer");
        String correctAnswer = launchedMe.getStringExtra("correctAnswer");
        int currentQuestion = launchedMe.getIntExtra("currentQuestion", 1);
        int totalCorrect = launchedMe.getIntExtra("totalCorrect", 1);
        displayInfo(itemValue, userAnswer, correctAnswer, currentQuestion, totalCorrect);
        resend(itemValue, userAnswer, correctAnswer, currentQuestion, totalCorrect);
    }

    private void displayInfo(String itemValue, String userAnswer, String correctAnswer, int currentQuestion, int totalCorrect) {
        TextView userAnswerPlace = (TextView)findViewById(R.id.userAnswer);
        TextView correctAnswerPlace = (TextView)findViewById(R.id.correctAnswer);
        TextView currentQuestionPlace = (TextView)findViewById(R.id.questionNum);
        TextView totalCorrectPlace = (TextView)findViewById(R.id.numCorrect);
        userAnswerPlace.setText(userAnswer);
        correctAnswerPlace.setText(correctAnswer);
        currentQuestionPlace.setText("Question Number " + currentQuestion + " out of 2");
        totalCorrectPlace.setText("You have " + totalCorrect + " out of " + currentQuestion + " correct answers!");
    }

    private void resend(final String itemValue, final String userAnswer, final String correctAnswer, final int currentQuestion, final int totalCorrect) {
        Button next = (Button)findViewById(R.id.next);
        if (currentQuestion == 2) {
            next.setText("Finish");
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nextActivity = new Intent(AnswerSummary.this, MainActivity.class);
                    startActivity(nextActivity);
                }
            });
        } else {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent nextActivity = new Intent(AnswerSummary.this, Questions.class);
                    nextActivity.putExtra("topics", itemValue);
                    nextActivity.putExtra("userAnswer", userAnswer);
                    nextActivity.putExtra("correctAnswer", correctAnswer);
                    nextActivity.putExtra("currentQuestion", currentQuestion);
                    nextActivity.putExtra("totalCorrect", totalCorrect);
                    nextActivity.putExtra("startQuestion", 10);
                    startActivity(nextActivity);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_answer_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
