package edu.washington.jamys2.quizdroid;

import android.app.Application;
import android.util.Log;

/**
 * Created by Anyone on 3/16/2015.
 */
public class QuizApp extends Application {

    private static QuizApp instance;
    private TopicRepos repository;

    @Override
    public void onCreate() {
        super.onCreate();
        this.repository = new TopicRepos();
        Log.d("QuizApp", "Application is running");
    }

    public QuizApp() {
        if (instance == null) {
            instance = this;
        } else {
            throw new RuntimeException("Multiple instances");
        }
    }

    public static QuizApp getInstance() {
        return instance;
    }

    public TopicRepos getRepository() {
        return repository;
    }
}
