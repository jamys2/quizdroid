package edu.washington.jamys2.quizdroid;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;


public class FragmentActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            TopicOverviewFragment overview = new TopicOverviewFragment();
            fragmentTransaction.add(R.id.container, overview);
            fragmentTransaction.commit();
        }
    }

    // Topic Overview
    public class TopicOverviewFragment extends Fragment {

        public TopicOverviewFragment() {
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_topic_overview, container, false);
            TextView topic_name = (TextView) rootView.findViewById(R.id.topic_id);
            TextView description = (TextView) rootView.findViewById(R.id.description);
            TextView question_num = (TextView) rootView.findViewById(R.id.question_num);
            getActivity().setTitle(QuizApp.getInstance().getRepository().getTitle());

            description.setText(QuizApp.getInstance().getRepository().getLong());
            topic_name.setText(QuizApp.getInstance().getRepository().getTitle());
            question_num.setText("4");

            Button b = (Button) rootView.findViewById(R.id.start);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fragmentManager = getActivity().getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    QuestionsFragment question = new QuestionsFragment();
                    fragmentTransaction.replace(R.id.container, question);
                    fragmentTransaction.commit();
                }
            });
            return rootView;
        }
    }

    // Questions
    public class QuestionsFragment extends Fragment {
        private Quiz current;
        private int answer;

        public QuestionsFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.activity_questions, container, false);
            getActivity().setTitle(R.string.title_activity_questions);

            answer = -1;

            current = QuizApp.getInstance().getRepository().getCurrentQuestion();
            final TextView questionView = (TextView) rootView.findViewById(R.id.question_title);
            questionView.setText(current.getQuestion());
            String[] getAnswer = current.getAnswers();

            Button b1 = (RadioButton) rootView.findViewById(R.id.radio1);
            b1.setText(getAnswer[0]);
            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answer = 0;
                    ((Button) rootView.findViewById(R.id.submit)).setEnabled(true);
                }
            });

            Button b2 = (RadioButton) rootView.findViewById(R.id.radio2);
            b2.setText(getAnswer[1]);
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answer = 1;
                    rootView.findViewById(R.id.submit).setEnabled(true);
                }
            });

            Button b3 = (RadioButton) rootView.findViewById(R.id.radio3);
            b3.setText(getAnswer[2]);
            b3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answer = 2;
                    rootView.findViewById(R.id.submit).setEnabled(true);
                }
            });

            Button b4 = (RadioButton) rootView.findViewById(R.id.radio4);
            b4.setText(getAnswer[3]);
            b4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answer = 3;
                    rootView.findViewById(R.id.submit).setEnabled(true);
                }
            });

            Button send = (Button) rootView.findViewById(R.id.submit);
            send.setEnabled(false);
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer != -1) {
                        if (answer == current.getCorrect()) {
                            QuizApp.getInstance().getRepository().incrementTotalCorrect();
                        }
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        AnswerSummaryFragment summary = new AnswerSummaryFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("theAnswer", answer);
                        summary.setArguments(bundle);
                        fragmentTransaction.replace(R.id.container, summary);
                        fragmentTransaction.commit();
                    }
                }
            });
            return rootView;
        }
    }

    // Answer Summary
    public class AnswerSummaryFragment extends Fragment {

        private boolean bool;

        public AnswerSummaryFragment(){
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.activity_answer_summary, container, false);
            getActivity().setTitle(R.string.title_activity_answer_summary);

            bool = false;
            int answerValue = getArguments().getInt("theAnswer");

            QuizApp instance = QuizApp.getInstance();
            ArrayList<Quiz> topics = instance.getRepository().getQuestions();

            Quiz currentQuestion = instance.getRepository().getCurrentQuestion();
            String[] theAnswers = currentQuestion.getAnswers();

            String userChoice = theAnswers[answerValue];
            String correctChoice = theAnswers[currentQuestion.getCorrect()];

            TextView userAnswerPlace = (TextView) rootView.findViewById(R.id.userAnswer);
            TextView correctAnswerPlace = (TextView) rootView.findViewById(R.id.correctAnswer);
            TextView currentQuestionPlace = (TextView) rootView.findViewById(R.id.questionNum);
            TextView totalCorrectPlace = (TextView) rootView.findViewById(R.id.numCorrect);

            userAnswerPlace.setText(userChoice);
            correctAnswerPlace.setText(correctChoice);
            currentQuestionPlace.setText("Question Number " + (instance.getRepository().getQuestionNumber() + 1) + " out of 4");
            totalCorrectPlace.setText("You have " + instance.getRepository().getTotalCorrect() + " out of "
                    + (instance.getRepository().getQuestionNumber() + 1) + " correct answers!");

            Button next = (Button) rootView.findViewById(R.id.next);
            if (topics.size() - 1 == instance.getRepository().getQuestionNumber()) {
                next.setText("Finish");
                bool = true;
            }
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!bool) {
                        QuizApp.getInstance().getRepository().incrementCurrentQuestion();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        QuestionsFragment question = new QuestionsFragment();
                        fragmentTransaction.replace(R.id.container, question);
                        fragmentTransaction.commit();
                    } else {
                        Intent nextActivity = new Intent(getActivity(), MainActivity.class);
                        startActivity(nextActivity);
                        getActivity().finish();
                    }
                }
            });

            return rootView;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
