package edu.washington.jamys2.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ListView Tutorial: http://androidexample.com/Create_A_Simple_Listview_-_Android_Example/index.php?view=article_discription&aid=65&aaid=90
        ListView listView = (ListView)findViewById(R.id.topic_list);
        Adapter adapter = new Adapter(this, R.layout.adapter, QuizApp.getInstance().getRepository().getTopics());
        listView.setAdapter(adapter);
        AdapterView.OnItemClickListener message = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                ListView listView = (ListView) view.getParent();
                Topic itemValue = (Topic)listView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "You have selected " + itemValue.getTitle(), Toast.LENGTH_SHORT).show();
                Intent nextActivity = new Intent(MainActivity.this, FragmentActivity.class);
                QuizApp.getInstance().getRepository().setCurrent(position);
                startActivity(nextActivity);
            }
        };
        listView.setOnItemClickListener(message);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
