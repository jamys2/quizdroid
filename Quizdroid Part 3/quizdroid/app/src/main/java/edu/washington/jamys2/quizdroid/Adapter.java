package edu.washington.jamys2.quizdroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Anyone on 3/16/15.
 */
public class Adapter extends ArrayAdapter<Topic> {
    Context mContext;
    public Adapter(Context context, int resource, List<Topic> topics) {
        super(context, resource, topics);
        mContext = context;

    }

    @Override
    public View getView(int position, View contextView, ViewGroup parent) {
        if (contextView == null) {
            contextView = LayoutInflater.from(mContext).inflate(R.layout.adapter, parent, false);
        }

        TextView topicTitle = (TextView) contextView.findViewById(R.id.topic_title);
        topicTitle.setText(getItem(position).getTitle());

        TextView topicShortDesc = (TextView) contextView.findViewById(R.id.topic_short_desc);
        topicShortDesc.setText(getItem(position).getShort());

        return contextView;
    }
}
