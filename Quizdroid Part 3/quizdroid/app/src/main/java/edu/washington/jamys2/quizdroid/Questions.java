package edu.washington.jamys2.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class Questions extends ActionBarActivity {

    int correct;
    int total;
    String[] math_questions;
    String[] physics_questions;
    String[] marvel_questions;
    String[] math_answers;
    String[] physics_answers;
    String[] marvel_answers;
    String[] math_correct;
    String[] physics_correct;
    String[] marvel_correct;
    TextView questionView;
    TextView answer1;
    TextView answer2;
    TextView answer3;
    TextView answer4;
    Button send;
    RadioGroup radioChoice;
    RadioButton radioChoiceButton;
    String itemValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);
        Intent launchedMe = getIntent();
        itemValue = launchedMe.getStringExtra("topics");
        int startQuestion = launchedMe.getIntExtra("startQuestion", 1);
        init();
        questions(startQuestion);
    }

    public void init() {
        math_questions = new String[] {"1 + 1 = ....", "10 - 1 = ...."};
        physics_questions = new String[] {"What scientist is well known for his theory of relativity?", "Earth is located in which galaxy?"};
        marvel_questions = new String[] {"Peter Parker works as a photographer for?", "Captain America was frozen in which war?"};

        math_answers = new String[] {"1", "2", "3", "4", "8", "9", "10", "11"};
        physics_answers = new String[] {"Albert Einstein", "Professor Xavier", "Charles Darwin", "None of the above", "Milky Way", "Andromeda", "Pinwheel", "Solar"};
        marvel_answers = new String[] {"The Daily Planet", "The Daily Bugle", "The New York Times", "The Rolling Stone", "World War I", "World War II", "Cold War", "American Civil War"};

        math_correct = new String[] {"2", "9"};
        physics_correct = new String[] {"Albert Einstein", "Milky Way"};
        marvel_correct = new String[] {"The Daily Bugle", "World War II"};

        send = (Button)findViewById(R.id.submit);
        questionView = (TextView)findViewById(R.id.question_title);
        answer1 = (TextView)findViewById(R.id.radio1);
        answer2 = (TextView)findViewById(R.id.radio2);
        answer3 = (TextView)findViewById(R.id.radio3);
        answer4 = (TextView)findViewById(R.id.radio4);

        send.setEnabled(false);
        radioChoice = (RadioGroup) findViewById(R.id.radioGroup);
        radioChoice.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId > 0) {
                    send.setEnabled(true);
                }
            }
        });
    }

    private void questions(int startQuestion) {
        if(startQuestion == 1) {
            startQuestion++;
            total = 0;
            correct = 0;
            switch (itemValue) {
                case "Math":
                    questionView.setText(math_questions[0]);
                    answer1.setText(math_answers[0]);
                    answer2.setText(math_answers[1]);
                    answer3.setText(math_answers[2]);
                    answer4.setText(math_answers[3]);
                    break;
                case "Physics":
                    questionView.setText(physics_questions[0]);
                    answer1.setText(physics_answers[0]);
                    answer2.setText(physics_answers[1]);
                    answer3.setText(physics_answers[2]);
                    answer4.setText(physics_answers[3]);
                    break;
                case "Marvel Super Heroes":
                    questionView.setText(marvel_questions[0]);
                    answer1.setText(marvel_answers[0]);
                    answer2.setText(marvel_answers[1]);
                    answer3.setText(marvel_answers[2]);
                    answer4.setText(marvel_answers[3]);
                    break;
            }
        } else {
            Intent change = getIntent();
            total = change.getIntExtra("currentQuestion", 1);
            correct = change.getIntExtra("totalCorrect", 1);
            switch (itemValue) {
                case "Math":
                    checkMath();
                    break;
                case "Physics":
                    checkPhysics();
                    break;
                case "Marvel Super Heroes":
                    checkMarvel();
                    break;
            }
        }

        send.setOnClickListener(new View.OnClickListener() {
            String correctAnswer = "";
            @Override
            public void onClick(View v) {
                total++;
                int selectedId = radioChoice.getCheckedRadioButtonId();
                radioChoiceButton = (RadioButton) findViewById(selectedId);
                if (selectedId > 0) {
                    Toast.makeText(getApplicationContext(), "You have selected " + radioChoiceButton.getText(), Toast.LENGTH_SHORT).show();
                    switch (itemValue) {
                        case "Math":
                            correctAnswer = correct();
                            break;
                        case "Physics":
                            correctAnswer = correct();
                            break;
                        case "Marvel Super Heroes":
                            correctAnswer = correct();
                            break;
                    }
                    radioChoice.clearCheck();
                }
                Intent nextActivity = new Intent(Questions.this, AnswerSummary.class);
                nextActivity.putExtra("topics", itemValue);
                nextActivity.putExtra("userAnswer", radioChoiceButton.getText());
                nextActivity.putExtra("correctAnswer", correctAnswer);
                nextActivity.putExtra("currentQuestion", total);
                nextActivity.putExtra("totalCorrect", correct);
                nextActivity.putExtra("startQuestion", 1);
                startActivity(nextActivity);
            }
        });
    }

    private void checkMath() {
        if ((total + 1) == math_questions.length) {
            questionView.setText(math_questions[total]);
            answer1.setText(math_answers[4]);
            answer2.setText(math_answers[5]);
            answer3.setText(math_answers[6]);
            answer4.setText(math_answers[7]);
        }
    }

    private void checkPhysics() {
        if ((total + 1) == physics_questions.length) {
            questionView.setText(physics_questions[total]);
            answer1.setText(physics_answers[4]);
            answer2.setText(physics_answers[5]);
            answer3.setText(physics_answers[6]);
            answer4.setText(physics_answers[7]);
        }
    }

    private void checkMarvel() {
        if ((total + 1) == marvel_questions.length) {
            questionView.setText(marvel_questions[total]);
            answer1.setText(marvel_answers[4]);
            answer2.setText(marvel_answers[5]);
            answer3.setText(marvel_answers[6]);
            answer4.setText(marvel_answers[7]);
        }
    }

    private String correct() {
        String[] answerCheck;
        switch (itemValue) {
            case "Math":
                answerCheck = math_correct;
                break;
            case "Physics":
                answerCheck = physics_correct;
                break;
            default:
                answerCheck = marvel_correct;
                break;
        }

        for (String anAnswerCheck : answerCheck) {
            if (anAnswerCheck.equals(radioChoiceButton.getText())) {
                correct++;
            }
        }
        return answerCheck[total - 1];
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_questions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
