Homework 6 - Quiz App (pt 1)
Homework requirement:

we want an application that will allow users to take multiple-choice quizzes


Stories:

As a user, when I start the app, I should see a list of different topics on which to take a quiz. (For now, these should be hard-coded to read "Math", "Physics", and "Marvel Super Heroes", as well as any additional topics you feel like adding into the mix.)
As a user, when I select a topic from the list, it should take me to the "topic overview" page, which displays a brief description of this topic, the total number of questions in this topic, and a "Begin" button taking me to the first question.
As a user, when I press the "Begin" button from the topic overview page, it should take me to the first question page, which will consist of a question (TextView), four radio buttons each of which consist of one answer, and a "Submit" button.
As a user, when I am on a question page and I press the "Submit" button, if no radio button is selected, it should do nothing. If a radio button is checked, it should take me to the "answer summary" page. (Ideally, the Submit button should not be visible until a radio button is selected.)
As a user, when I am on the "answer summary" page, it should display the answer I gave, the correct answer, and tell me the total number of correct vs incorrect answers so far ("You have 5 out of 9 correct"), and display a "Next" button taking me to the next question page, or else display a "Finish" button if that is the last question in the topic.
 

Homework Grading:

All your code should be in a GitHub repo under your account
repo should be called 'quizdroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator
pic or screenshot or movie of app running on a device
We will clone and build it from the GH repo
5 points, one for each satisfied story
BONUS:
if I hit the "back" button on a question page, it should NOT take me to the answer summary page of the previous question, but to the previous question page directly. Similarly, hitting "back" from the first question should take me back to the topic list page, not the topic overview page. (1 pt)


----

Homework 7 - Quiz App (pt 2)
Homework requirement:

in this part, switch from using Activities to Fragments


Stories:

As a developer, the "topic overview", "question" and "answer" pages should be merged into a single Activity, using Fragments to swap between the question UI and the answer UI. This means, then, that there will be only two Activities in the entire system: the list of topics at the front of the application, and the multi-use activity that serves for the topic overview, the question, and answer pages.
 
Homework Grading:

All your code should be in a GitHub repo under your account
repo should be called 'quizdroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator
pic or screenshot or movie of app running on a device
We will clone and build it from the GH repo
5 points, one for each satisfied story from the previous (pt1) version of the assignment refactored to use fragments
BONUS:
use animation transitions to "slide" the fragments and and out; when going from question to answer, for example, slide the answer fragment in from the right towards the left. (1 pt)

----

Homework 8 - Quiz App (pt 3)
Homework requirement:

An application that will allow users to take multiple-choice quizzes
now we will refactor to use a domain model and an Application object

note that a future version of this codebase will require permissions to be set; this can be done now or later, as you wish


Stories:

As a developer, create a class called QuizApp extending android.app.Application and make sure it is referenced from the app manifest; override the onCreate() method to emit a message to the diagnostic log to ensure it is being loaded and run
As a developer, use the "Repository" pattern to create a TopicRepository interface; create one implementation that simply stores elements in memory from a hard-coded list initialized on startup. Create domain objects for Topic and Quiz, where a Quiz is question text, four answers, and an integer saying which of the four answers is correct, and Topic is a title, short description, long description, and a collection of Question objects.
As a developer, make the QuizApp object a singleton, and provide a method for accessing the TopicRepository.
As a developer, refactor the activities in the application to use the TopicRepository. On the topic list page, the title and the short description should come from the similar fields in the Topic object. On the topic overview page, the title and long description should come from the similar fields in the Topic object. The Question object should be similarly easy to match up to the UI.
 
Homework Grading:

All your code should be in a GitHub repo under your account
repo should be called 'quizdroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator
pic or screenshot or movie of app running on a device
We will clone and build it from the GH repo
5 points, one for each satisfied story
BONUS:
In the next part, we will need this application to need to access the Internet, among other things. Look through the list of permissions in the Android documentation, and add uses-permission elements as necessary to enable that now. (1 pt)
Refactor the domain model so that Topics can have an icon to go along with the title and descriptions. (Use the stock Android icon for now if you don't want to test your drawing skills.) Refactor the topic list ListView to use the icon as part of the layout for each item in the list view. Display the icon on the topic overview page.


----


Homework 10 - Quiz App (pt 4)
Homework requirement:

An application that will allow users to take multiple-choice quizzes
now we will write the code to check the questions periodically, store the data, and allow for preferences


Stories:

As a user, the application should provide a "Preferences" action bar item that brings up a "Preferences" activity containing the application's configurable settings: URL to use for question data, and how often to check for new downloads, the "N" (minutes/hours) in the previous story. If a download is currently under way, these settings should not take effect until the next download starts.
As a developer, the application should create some background operation (Thread, AlarmManager or Service) that will (eventually) attempt to download a JSON file containing the questions from the server every "N" minutes/hours. For now, pop a Toast message displaying the URL that will eventually be hit. Make sure this URL is what's defined in the Preferences.
 
Homework Grading:

All your code should be in a GitHub repo under your account
repo should be called 'quizdroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator
pic or screenshot or movie of app running on a device


-----


Homework 11 - Quiz App (pt 5)
Homework requirement:

An application that will allow users to take multiple-choice quizzes
now we will download the topics and questions from a server


Stories:

As a developer, when the application starts up, the TopicRepository should read the Topics and Questions for the application from a local file called "quizdata.json".
As a developer, the background operation implemented previously should now attempt to download the JSON file specified in the Preferences URL. Save this data to a local file as "quizdata.json". Make sure that this file always remains in a good state--if the download fails or is interrupted, the previous file should remain in place.
As a user, if I am currently offline (in Airplane mode or in a no-bars area) the application should display a message telling me I have no access to the Internet. If I am in airplane mode, it should ask if I want to turn airplane mode off and take me to the Settings activity to do so. If I simply have no signal, it should just punt gracefully with a nice error message.
As a user, if the download of the questions fails, the application should tell me and ask if I want to retry or quit the application and try again later.
 
Homework Grading:

All your code should be in a GitHub repo under your account
repo should be called 'quizdroid'
repo should contain all necessary build artifacts
include a directory called "screenshots", including:
screenshot of app running on emulator 
pic or screenshot or movie of app running on a device