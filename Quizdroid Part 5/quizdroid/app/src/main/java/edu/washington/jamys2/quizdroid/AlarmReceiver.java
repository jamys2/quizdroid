package edu.washington.jamys2.quizdroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AlarmReceiver", "Alarm is active!");
        QuizApp instance = QuizApp.getInstance();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        ArrayList<String> preferences = instance.getPreferences();
        if (instance.getChanged()) {
            instance.setPreferences(0, shared.getString("frequency", "5"));
            instance.setPreferences(1, shared.getString("downloadURL", ""));
            instance.setChanged(false);
            instance.start();
        }
        String message = preferences.get(1) + ": " + preferences.get(0);//intent.getStringExtra();
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
