package edu.washington.jamys2.quizdroid;

import android.util.JsonReader;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/15.
 */
public class JsonHelper {
    public static ArrayList<Topic> send(JsonReader jsonReader) throws IOException {
        ArrayList<Topic> topics = new ArrayList<>();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            jsonReader.beginObject();
            Topic topic = new Topic();
            if (jsonReader.nextName().equals("title")) {
                topic.setTitle(jsonReader.nextString());
            }
            if (jsonReader.nextName().equals("desc")) {
                topic.setShort(jsonReader.nextString());
            }
            if (jsonReader.nextName().equals("questions")) {
                ArrayList<Quiz> quizzes = new ArrayList<>();
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    jsonReader.beginObject();
                    Quiz quiz = new Quiz();
                    if (jsonReader.nextName().equals("text"))
                        quiz.setQuestion(jsonReader.nextString());
                    if (jsonReader.nextName().equals("answer"))
                        quiz.setCorrect(Integer.parseInt(jsonReader.nextString()) - 1);
                    if (jsonReader.nextName().equals("answers")) {
                        jsonReader.beginArray();
                        while (jsonReader.hasNext()) {
                            quiz.addAnswer(jsonReader.nextString());
                        }
                        jsonReader.endArray();
                    }
                    jsonReader.endObject();
                    quizzes.add(quiz);
                }
                jsonReader.endArray();
                topic.setQuestions(quizzes);
            }
            jsonReader.endObject();
            topics.add(topic);
        }
        jsonReader.endArray();
        return topics;
    }
}
