package edu.washington.jamys2.quizdroid;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/2015.
 */
public class Quiz implements Serializable {

    private ArrayList<String> answers;
    private String question;
    private int correct;

    public Quiz() {
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public void addAnswer(String s) {

    }
}
