package edu.washington.jamys2.quizdroid;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Anyone on 3/16/2015.
 */
public class TopicRepos implements TopicRepository {

    private ArrayList<Topic> topics;
    private int current;

    public TopicRepos() {
        topics = Questions();
    }

    public ArrayList<Topic> Questions() {
        Topic math = setTopic("Math");
        Topic physics = setTopic("Physics");
        Topic marvel = setTopic("Marvel");
        return new ArrayList<>(Arrays.asList(math, physics, marvel));
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }

    private Topic setTopic(String category) {
        Topic topic = new Topic();
        Quiz question = new Quiz();
        ArrayList<Quiz> list = new ArrayList<>();
        switch (category) {
            case "Math":
                topic.setTitle("Math");
                topic.setLong("Test your mathematical abilities through a series of questions!" +
                        " At the end of the quiz, you will receive your results. Begin the quiz now!");
                topic.setShort("Are You a Math Wizard?");

                question.setQuestion("1 + 1 = ....");
                question.setAnswers(new ArrayList<>(Arrays.asList("1", "2", "3", "4")));
                question.setCorrect(1);
                list.add(question);
                question = new Quiz();

                question.setQuestion("10 - 1 = ....");
                question.setAnswers(new ArrayList<>(Arrays.asList("8", "9", "10", "11")));
                question.setCorrect(1);
                list.add(question);
                question = new Quiz();

                question.setQuestion("10 * 10 = ....");
                question.setAnswers(new ArrayList<>(Arrays.asList("1", "10", "100", "1000")));
                question.setCorrect(1);
                list.add(question);
                question = new Quiz();
                topic.setQuestions(list);

                question.setQuestion("0 / 1 = ....");
                question.setAnswers(new ArrayList<>(Arrays.asList("0", "1", "2", "Invalid, cannot divide")));
                question.setCorrect(0);
                list.add(question);
                question = new Quiz();
                break;
            case "Physics":
                topic.setTitle("Physics");
                topic.setLong("Are you confident in your physics knowledge? Well, what are you" +
                        " waiting for, start your test now!");
                topic.setShort("Are You Einstein's Decedent?");

                question.setQuestion("What scientist is well known for his theory of relativity?");
                question.setAnswers(new ArrayList<>(Arrays.asList("Albert Einstein", "Professor Xavier", "Charles Darwin", "None of the above")));
                question.setCorrect(0);
                list.add(question);
                question = new Quiz();

                question.setQuestion("Earth is located in which galaxy?");
                question.setAnswers(new ArrayList<>(Arrays.asList("Milky Way", "Andromeda", "Pinwheel", "Solar")));
                question.setCorrect(0);
                list.add(question);
                question = new Quiz();

                question.setQuestion("What kind of eclipse do we have when the moon is between the sun and the earth?");
                question.setAnswers(new ArrayList<>(Arrays.asList("A Lunar Eclipse", "A Center Eclipse", "A New Eclipse", "A Solar Eclipse")));
                question.setCorrect(3);
                list.add(question);
                question = new Quiz();
                topic.setQuestions(list);

                question.setQuestion("When light bends as it enters a different medium the process is known as what?");
                question.setAnswers(new ArrayList<>(Arrays.asList("Reflection", "Diffraction", "Luminescent Pulling", "Refraction")));
                question.setCorrect(3);
                list.add(question);
                question = new Quiz();
                break;
            default:
                topic.setTitle("Marvel Super Heroes ");
                topic.setLong("Love Marvel? Enjoy the comics? The movies? Will you be willing" +
                        " to test your knowledge and confidence today? Begin now!!");
                topic.setShort("Are You a Super Hero Fan?");

                question.setQuestion("Peter Parker works as a photographer for?");
                question.setAnswers(new ArrayList<>(Arrays.asList("The Daily Planet", "The Daily Bugle", "The New York Times", "The Rolling Stone")));
                question.setCorrect(1);
                list.add(question);
                question = new Quiz();

                question.setQuestion("Captain America was frozen in which war?");
                question.setAnswers(new ArrayList<>(Arrays.asList("World War I", "World War II", "Cold War", "American Civil War")));
                question.setCorrect(1);
                list.add(question);
                question = new Quiz();

                question.setQuestion("What kind of eclipse do we have when the moon is between the sun and the earth?");
                question.setAnswers(new ArrayList<>(Arrays.asList("A Lunar Eclipse", "A Center Eclipse", "A New Eclipse", "A Solar Eclipse")));
                question.setCorrect(3);
                list.add(question);
                question = new Quiz();
                topic.setQuestions(list);

                question.setQuestion("When light bends as it enters a different medium the process is known as what?");
                question.setAnswers(new ArrayList<>(Arrays.asList("Reflection", "Diffraction", "Luminescent Pulling", "Refraction")));
                question.setCorrect(3);
                list.add(question);
                question = new Quiz();
                break;
        }
        return topic;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    @Override
    public ArrayList<Quiz> getQuestions() {
        return topics.get(current).getQuestions();
    }

    @Override
    public void setQuestions(ArrayList<Quiz> questions) {
        topics.get(current).setQuestions(questions);
    }

    @Override
    public String getTitle() {
        return topics.get(current).getTitle();
    }

    @Override
    public void setTitle(String title) {
        topics.get(current).setTitle(title);
    }

    @Override
    public String getShort() {
        return topics.get(current).getShort();
    }

    @Override
    public void setShort(String shortDescription) {
        topics.get(current).setTitle(shortDescription);
    }

    @Override
    public String getLong() {
        return topics.get(current).getLong();
    }

    @Override
    public void setLong(String longDescription) {
        topics.get(current).setLong(longDescription);
    }

    @Override
    public Quiz getCurrentQuestion() {
        return topics.get(current).getCurrentQuestion();
    }

    @Override
    public int getQuestionNumber() {
        return topics.get(current).getQuestionNumber();
    }

    @Override
    public void incrementCurrentQuestion() {
        topics.get(current).incrementQuestion();
    }

    @Override
    public int getTotalCorrect() {
        return topics.get(current).getCorrect();
    }

    @Override
    public void incrementTotalCorrect() {
        topics.get(current).incrementCorrect();
    }
}
