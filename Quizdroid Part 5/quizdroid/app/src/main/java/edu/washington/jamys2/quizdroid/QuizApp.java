package edu.washington.jamys2.quizdroid;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Anyone on 3/16/2015.
 */
public class QuizApp extends Application {

    private static QuizApp instance;
    private PendingIntent pendingIntent;
    private TopicRepos repository;
    private ArrayList<String> preferences = new ArrayList<>();
    private ArrayList<Topic> topics = new ArrayList<>();
    private boolean changed;
    private boolean on;
    private RequestQueue requestQueue;

    @Override
    public void onCreate() {
        super.onCreate();

        boolean airplane = isAirplaneModeOn(this);

        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (airplane) {
            Log.d("QuizApp", "Airplane mode is on");
            Toast.makeText(this, "Please click on the app settings to configure airplane mode", Toast.LENGTH_LONG).show();
        } else if (!mWifi.isConnected()) {
            Log.d("QuizApp", "Wifi Not On");
            Toast.makeText(this, "Please check your wireless", Toast.LENGTH_SHORT).show();
        }


        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        preferences.add(sharedPref.getString("frequency", "5"));
        preferences.add(sharedPref.getString("downloadURL", ""));

        Intent alarmIntent = new Intent(QuizApp.this, AlarmReceiver.class);

        pendingIntent = PendingIntent.getBroadcast(QuizApp.this, 1,
                alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        request();
        start();
        read();
        this.repository = new TopicRepos();
        Log.d("QuizApp", "Application is running");
    }

    private void request() {
        String address = "http://tednewardsandbox.site44.com/questions.json";
        JsonArrayRequest jsArrayRequest = new JsonArrayRequest(address, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                write(response.toString());
                failed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("exception", "Response Error: " + error.toString());
            }
        });
    }

    private void failed() {
        new AlertDialog.Builder(this).setTitle("Download has failed").setMessage("Try Aagain?").show();
    }

    private void write(String info) {
        Log.i("QuizApp", "Writing to quizdata.json");
        try {
            File filePath = new File(this.getFilesDir().getAbsolutePath() + "/quizdata.json");
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(info.getBytes());
            fos.close();

            Log.i("info", "Wrote to file");
        } catch (IOException e) {
            Log.e("exception", "File write failed: " + e.toString());
        }
    }

    public void read() {
        try {
            FileInputStream read = openFileInput("quizdata.json");
            JsonReader jsonReader = new JsonReader(new InputStreamReader(read));
            ArrayList<Topic> topicsList = JsonHelper.send(jsonReader);
            topics.addAll(topicsList);
            jsonReader.close();
            read.close();
        } catch (IOException e) {
            Log.e("QuizApp", "File not found");
        }
    }

    public QuizApp() {
        if (instance == null) {
            instance = this;
        } else {
            throw new RuntimeException("Multiple instances");
        }
    }

    public static QuizApp getInstance() {
        return instance;
    }

    public TopicRepos getRepository() {
        return repository;
    }

    public ArrayList<String> getPreferences() {
        return preferences;
    }

    public boolean getChanged() {
        return changed;
    }

    public void setPreferences(int i, String frequency) {
        preferences.set(i, frequency);
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public void setDownloading(boolean setting) {
        on = setting;
    }

    public boolean getDownloading() {
        return on;
    }

    public void start() {
        setDownloading(true);
        int interval = Integer.parseInt(preferences.get(0)) * 1000;// * 60; //Converts min to milli;

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
    }

    public void cancel() {
        setDownloading(false);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        pendingIntent.cancel();
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }

    @SuppressWarnings("deprecation")
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(), android.provider.Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }
}
